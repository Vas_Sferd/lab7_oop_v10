#include "pch.h"

using namespace System;

class Student {
    std::wstring name;
public:
    Student(std::wstring name) : name(name) {}
    bool operator<(const Student& other) const {
        return this->name < other.name;
    }
    friend std::wostream& operator<<(std::wostream& os, const Student& self) {
        return os << self.name;
    }
};

class Group {
    std::wstring name;
    std::vector<Student> students;
public:
    Group(std::wstring name) : name(name) {}
    std::wstring getName() const { return name; }  
    int studentsCount() const { return students.size(); }
    bool operator<(const Group& other) const {
        return this->name < other.name;
    }
    std::wstring addStudent(Student student) {
        students.push_back(student);
        std::sort(students.begin(), students.end());
    }
    std::wstring deleteStudent(int position) {
        std::wostringstream res;

        if (position < 1 || position > students.size()) {
            res << "������ �������� ����� ������� ��� �������� �������� �� ������ ������ (" << position << ")" << std::endl;
            return res.str();
        }
        int index = position - 1;
        students.erase(students.begin() + index);
        res << "������� ������ ���������" << std::endl;
        return res.str();
    }
    friend std::wostream& operator<<(std::wostream& os, const Group& self) {
        os << self.name;
        for (int i = 0; i < self.students.size(); ++i) {
            os << "\t" << (i + 1) << ": " << self.students[i] << std::endl;
        }
        return os;
    }
};

class Cafedra {
    std::vector<Group> groups;
public:
    Cafedra() {}
    int groupsCount() const { return groups.size(); }
    std::wstring getGroupName(int position) {
        if (position < 1 || position > groups.size()) {
            return L"!!!��� ������!!!";
        } else {
            return groups[position - 1].getName();
        }
    }
    std::wstring addGroup(Group group) {
        groups.push_back(group);
        std::sort(groups.begin(), groups.end());
    }
    std::wstring deleteGroup(int position) {
        std::wostringstream res;
        if (position < 1 || position > groups.size()) {
            res << "������ �������� ����� ������� ��� �������� �� ������ ����� ������� (" << position << ")" << std::endl;
            return res.str();
        }
        int index = position - 1;
        groups.erase(groups.begin() + index);
        res << "������ ������� ���������" << std::endl;
        return res.str();
    }
    friend std::wostream& operator<<(std::wostream& os, const Cafedra& self) {
        os << "������ �������:" << std::endl;
        for (int i = 0; i < self.groups.size(); ++i) {
            os << (i + 1) << ": " << self.groups[i] << std::endl;
        }
        return os;
    }
};

ref class ConsoleWriter {
public:
    ConsoleWriter() {}
    void write(std::wstring message) {
        std::wcout << message.c_str() << std::endl;;
    }
};

ref class FileWriter {
    std::wofstream* wout;
public:
    FileWriter(std::wstring path) { wout = new std::wofstream; wout->open("log.txt"); }
    void write(std::wstring message) {
        (*wout) << message;
    }
};

delegate void CafedraEvent(const Cafedra& cafedra);  
delegate void GroupEvent(const Group& group);
delegate void MessageEvent(const std::wstring);
delegate void Signal();

ref class Program {
public:
    event CafedraEvent^ onDeleteGroup;
    event GroupEvent^ onStudentAdd;
    event Signal^ onAllGroupsDeleted;
    event GroupEvent^ onGroupNotSmallAnymore;
    event MessageEvent^ onMessageSend;
    event MessageEvent^ onLogSend;

    void checkIfAllGroupsDeleted(const Cafedra& cafedra) {
        if (cafedra.groupsCount() == 0) {
            onAllGroupsDeleted();
        }
    }
    
    void checkIfGroupNotSmallAnymore(const Group& group) {
        if (group.studentsCount() > 5) {
            onGroupNotSmallAnymore(group);
        }
    }

    void logAllGroupsDeleted() {
        onLogSend(L"������ ����� ������� ������ ����");
    }
    
    void logGroupNotSmallAnymore(const Group& group) {
        std::wostringstream message;
        message << "������ � ������ " << group.getName() << " ������ ���� �������";
        onLogSend(message.str());
    }

    void writeAllLog(const std::wstring message) {
        onMessageSend(L"[Log] " + message);
    }

    Program() {}
    void run() {
        onDeleteGroup += gcnew CafedraEvent(this, &Program::checkIfAllGroupsDeleted);
        onStudentAdd += gcnew GroupEvent(this, &Program::checkIfGroupNotSmallAnymore);
        onAllGroupsDeleted += gcnew Signal(this, &Program::logAllGroupsDeleted);
        onGroupNotSmallAnymore += gcnew GroupEvent(this, &Program::logGroupNotSmallAnymore);
        onLogSend += gcnew MessageEvent(this, &Program::writeAllLog);

        Cafedra cafedra;
        int current = 0;

        while (true) {
            std::wstring greeting;
            if (current == 0) {
                greeting = L"������� (";
                greeting = greeting + std::to_wstring(cafedra.groupsCount()) + L" �����)" + L" :>> ";
            } else {
                greeting = cafedra.getGroupName(current) + L" :>> ";
            }
            onMessageSend(L"\n" + greeting);
            
            std::wstringstream command_stream;

            std::wstring line;
            std::getline(std::wcin, line);
            command_stream << line;
            
            std::wstring command;
            command_stream >> command;

            if (command == L"��������") {
                std::wostringstream out;
                out << cafedra;
            }

            onMessageSend(command);
        }
    }
};

int main()
{
    std::locale::global(std::locale(""));
    std::wcin.imbue(std::locale(".866"));

    System::Console::WriteLine("��������� �����������...");
    Program program;
    
    std::cout << "������";

    ConsoleWriter^ consoleWriter = gcnew ConsoleWriter();
    FileWriter^ fileWriter = gcnew FileWriter(L"log.txt");
    
    MessageEvent^ wconsole = gcnew MessageEvent(consoleWriter, &ConsoleWriter::write);
    MessageEvent^ wfile = gcnew MessageEvent(fileWriter, &FileWriter::write);

    program.onLogSend += wconsole;
    program.onLogSend += wfile;

    program.onMessageSend += wconsole;

    program.run();
    return 0;
}